const express = require('express');
const Events = require("../models/Events");
const auth = require("../middleware/auth");
const User = require("../models/User");

const router = express.Router();


router.get('/', auth, async (req, res) => {
  try {
    const userId = req.user._id;
    const user = await User.findById(userId)
    let events = [];
    await Promise.all(user.friends.map(async friend => {
      const friendEvents = await Events.find({host: friend}).populate('host', 'displayName');
      events = [...events, ...friendEvents];
    }));
    const userEvents = await Events.find({host: userId}).populate('host', 'displayName');
    events = [...events, ...userEvents];
    events.sort((a, b) => a.date.getTime() - b.date.getTime());
    res.send(events);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/', auth, async (req, res) => {
  try {
    await Events.findOneAndDelete({_id: req.body.id});
    res.send({message: "Event deleted"});
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post('/', auth, async (req, res) => {
  try {
    const eventData = {
      name: req.body.name,
      date: req.body.date,
      duration: req.body.duration,
      host: req.user._id
    }
    const event = new Events(eventData);
    await event.save();
    res.send(event);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;