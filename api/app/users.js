const express = require('express');
const User = require('../models/User');
const config = require('../config');
const axios = require("axios");
const auth = require("../middleware/auth");
const {nanoid} = require('nanoid');
const {OAuth2Client} = require('google-auth-library')


const googleClient = new OAuth2Client(config.google.clientId)
const router = express.Router();

router.post('/', async (req, res) => {
  try {
    const user = new User({
      email: req.body.email,
      password: req.body.password,
      displayName: req.body.displayName
    });
    user.generateToken();
    await user.save();
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post('/friends', auth, async (req, res) => {
  try {
    const userId = req.user._id;
    const user = await User.findById(userId);
    const friend = await User.findOne({email: req.body.email});
    if (!friend) {
      return res.status(401).send({error: 'Wrong email'});
    }

    if (userId.toString() === friend._id.toString()) {
      return res.status(401).send({error: 'Can not friend yourself'});
    }

    if (user.friends.includes(friend._id)) {
      return res.send({message: 'User is already your friend'});
    }

    user.friends.push(friend._id);
    await user.save();
    res.send({message: 'User was added as a friend'});
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.delete('/friends', auth, async (req, res) => {
  try {
    const userId = req.user._id;
    const user = await User.findById(userId);
    const friend = await User.findById(req.body.id);

    if (!friend || !user.friends.includes(friend._id)) {
      return res.status(401).send({error: 'Wrong email'});
    }
    const newFriendList = user.friends.filter(item => item.toString()!==req.body.id.toString());
    user.friends = newFriendList;
    await user.save();
    res.send({message: 'User was added as a friend'});
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get('/friends', auth, async (req, res) => {
  try {
    const userId = req.user._id;
    const user = await User.findById(userId).populate('friends', 'displayName');
    res.send(user.friends);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({email: req.body.email});

  if (!user) {
    return res.status(401).send({error: 'Email or password is wrong'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({error: 'Email or password is wrong'});
  }

  user.generateToken();
  await user.save();
  return res.send({message: 'Email and password are correct', user});
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};
  if (!token) return res.send(success);
  const user = await User.findOne({token});
  if (!user) return res.send(success);
  user.generateToken();
  await user.save();
  return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);

    if (response.data.data.error) {
      return res.status(401).send({message: 'Facebook token incorrect'});
    }

    if (response.data.data['user_id'] !== req.body.id) {
      return res.status(401).send({message: 'User id incorrect'});
    }

    let user = await User.findOne({email: req.body.email});

    if (!user) {
      user = await User.findOne({facebookId: req.body.id})
    }

    if (!user) {
      user = new User({
        email: req.body.email || nanoid(),
        password: nanoid(),
        facebookId: req.body.id,
        displayName: req.body.name,
      });
    }

    user.generateToken();
    await user.save();

    res.send({message: 'Success', user});
  } catch (e) {
    res.status(401).send({global: 'Facebook token incorrect'});
  }
});

router.post('/googleLogin', async (req, res) => {
  try {
    const ticket = await googleClient.verifyIdToken({
      idToken: req.body.tokenId,
      audience: config.google.clientId
    });

    const {name, email, sub: ticketUserId} = ticket.getPayload();

    if (req.body.googleId !== ticketUserId) {
      return res.status(401).send({global: "User ID incorrect"});
    }

    let user = await User.findOne({email});

    if (!user) {
      user = new User({
        email,
        password: nanoid(),
        displayName: name,
      });
    }

    user.generateToken();
    await user.save();
    res.send({message: 'Success', user});
  } catch (e) {
    res.status(500).send({global: 'Server error, please, try again '});


  }

});

module.exports = router;