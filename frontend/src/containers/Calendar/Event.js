import React from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import makeStyles from "@material-ui/core/styles/makeStyles";
import Typography from "@material-ui/core/Typography";
import {Button} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {deleteEventsRequest} from "../../store/actions/eventsActions";

const useStyles = makeStyles({
  card: {
    height: '100%'
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  content: {
    flex: '1 0 auto',
  },
});


const Event = ({name, date, duration, host, userId, friendId, id}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  let hide = false;
  let disableBtn = true;

  if (userId === friendId) {
    disableBtn = false;
  }


  const dateTime = new Date(date);
  if (dateTime < new Date()) {
    hide = true
  }
  const dateForm = dateTime.getDate() + "/"
    + (dateTime.getMonth() + 1) + "/"
    + dateTime.getFullYear() + " at "
    + dateTime.getHours() + ":"
    + dateTime.getMinutes();

  const deleteEvent = () => {
    dispatch(deleteEventsRequest({id}));
  };

  return (
    <>{!hide ? (
      <Grid item xs={12} sm={12} md={4} lg={4}>
        <Card className={classes.card}>
          <CardHeader title={name}/>
          <CardContent className={classes.content}>
            <Typography component="h5" variant="h5">
              <strong>
                Date: {dateForm}
              </strong>
            </Typography>
            <Typography component="h5" variant="h5">
              Duration: {duration}
            </Typography>
            <Typography component="h5" variant="h5">
              Hosted by: {host}
            </Typography>
          </CardContent>
          <CardActions>
            <IconButton disabled={disableBtn} onClick={deleteEvent} component={Button}>
              <DeleteIcon/>
            </IconButton>
          </CardActions>
        </Card>
      </Grid>) : <></>
    }
    </>
  );
};


Event.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  duration: PropTypes.number.isRequired,
};

export default Event;