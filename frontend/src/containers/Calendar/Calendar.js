import React, {useEffect} from 'react';
import history from "../../history";
import {clearErrorRequest} from "../../store/actions/usersActions";
import {useDispatch, useSelector} from "react-redux";
import HomeLayout from "../../components/UI/Layout/HomeLayout";
import {Link} from "react-router-dom";
import {Button, CircularProgress, Grid, makeStyles, Typography} from "@material-ui/core";
import Event from "./Event";
import {eventsRequest} from "../../store/actions/eventsActions";

const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  },
  head: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  }
}));


const Calendar = () => {
  const dispatch = useDispatch();
  history.listen((location) => {
    if (location.pathname === '/login' || location.pathname === '/register') {
      dispatch(clearErrorRequest());
    }
  });
  const classes = useStyles();
  const events = useSelector(state => state.events.events);
  const loading = useSelector(state => state.events.eventsLoading);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    dispatch(eventsRequest());
  }, [dispatch]);


  return (
    <HomeLayout>
      <Grid container direction="column" spacing={2}>
        <Grid item className={classes.head}>
          <Typography variant='h5'>{user.displayName}'s calendar</Typography>
          <Button color="primary" component={Link} to="/new-event">Add event</Button>
        </Grid>
        <Grid item container spacing={1}>
          {loading ? (
            <Grid container justify='center' alignContent="center" className={classes.progress}>
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>) : events.map(event => (
            <Event
              key={event._id}
              id={event._id}
              userId={user._id}
              friendId={event.host._id}
              name={event.name}
              date={event.date}
              duration={event.duration}
              host={event.host.displayName}
            />
          ))}
        </Grid>
      </Grid>
    </HomeLayout>
  );
};

export default Calendar;