import React, {useEffect} from 'react';
import {Button, CircularProgress, Grid, makeStyles} from "@material-ui/core";
import Modal from "@material-ui/core/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import AppDrawer from "../../components/UI/Layout/AppDrawer";
import FriendsFrom from "../../components/FriendsFrom/FriendsFrom";
import {deleteFriendsRequest, friendsRequest} from "../../store/actions/friendsActions";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  },
  item: {
    margin: '5px',
    display: "flex",
    justifyContent: "space-between"
  }
}));

const Friends = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const friends = useSelector(state => state.friends.friends);
  const loading = useSelector(state => state.friends.friendsLoading);


  useEffect(() => {
    dispatch(friendsRequest());
  }, [dispatch]);

  const unFriend = (id) => {
    dispatch(deleteFriendsRequest({id}));
  };


  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  return (
    <AppDrawer>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {<FriendsFrom
          setOpen={setOpen}
        />}
      </Modal>
      <Button color="primary" onClick={handleOpen}>Add friend</Button>
      <Grid container>
        {loading ? (
          <Grid container justify='center' alignContent="center" className={classes.progress}>
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>) : friends.map(friend => (
          <Grid item className={classes.item} xs={12} key={friend._id}>
            {friend.displayName}
            <Button color="primary" onClick={() => unFriend(friend._id)}>{<HighlightOffIcon/>}</Button>
          </Grid>
        ))}
      </Grid>


    </AppDrawer>
  );
};

export default Friends;