import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import EventForm from "../../components/EventForm/EventForm";
import {postEventsRequest} from "../../store/actions/eventsActions";

const NewEvent = () => {
  const dispatch = useDispatch();
  const error = useSelector(state => state.events.postEventsError);
  const loading = useSelector(state => state.events.postEventsLoading);

  const onProductsFormSubmit = event => {
    dispatch(postEventsRequest(event));
  };


  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h5'>
          New event
        </Typography>
      </Grid>
      <Grid item xs>
        <EventForm
          onSubmit={onProductsFormSubmit}
          loading={loading}
          error={error}
        />
      </Grid>
    </Grid>
  );
};

export default NewEvent;