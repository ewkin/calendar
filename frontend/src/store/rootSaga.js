import {all} from 'redux-saga/effects';
import history from "../history";
import historySagas from "./sagas/historySagas";
import userSagas from "./sagas/userSagas";
import eventsSagas from "./sagas/eventsSagas";
import friendsSagas from "./sagas/friendsSagas";

export default function* rootSaga() {
  yield all([
    ...historySagas(history),
    ...userSagas,
    ...eventsSagas,
    ...friendsSagas
  ])
};