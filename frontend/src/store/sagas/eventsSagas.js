import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {NotificationManager} from "react-notifications";
import {
  deleteEventsFailure,
  deleteEventsRequest,
  deleteEventsSuccess,
  eventsFailure,
  eventsRequest,
  eventsSuccess,
  postEventsFailure,
  postEventsRequest,
  postEventsSuccess
} from "../actions/eventsActions";
import {historyPush} from "../actions/historyActions";

export function* fetchEvents() {
  try {
    const response = yield axiosApi.get('/events');
    yield put(eventsSuccess(response.data));
  } catch (e) {
    NotificationManager.error(e.response.data.error || e.response.data.global || e.response.data)
    yield put(eventsFailure(e.response.data));
  }
}

export function* postEvents({payload: data}) {
  try {
    yield axiosApi.post('/events', data);
    yield put(postEventsSuccess());
    yield put(historyPush('/'));
  } catch (e) {
    yield put(postEventsFailure(e.response.data));
  }
}

export function* deleteEvents({payload: data}) {
  try {
    const response = yield axiosApi.delete('/events', {data});
    NotificationManager.success(response.data.message);
    yield put(deleteEventsSuccess());
    yield put(eventsRequest())
  } catch (e) {
    yield put(deleteEventsFailure(e.response.data));
  }
}

const eventsSagas = [
  takeEvery(eventsRequest, fetchEvents),
  takeEvery(postEventsRequest, postEvents),
  takeEvery(deleteEventsRequest, deleteEvents)
];

export default eventsSagas