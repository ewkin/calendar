import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {NotificationManager} from "react-notifications";
import {historyPush} from "../actions/historyActions";
import {
  deleteFriendsRequest, deleteFriendsSuccess,
  friendsFailure,
  friendsRequest,
  friendsSuccess,
  postFriendsFailure, postFriendsRequest,
  postFriendsSuccess
} from "../actions/friendsActions";
import {eventsRequest} from "../actions/eventsActions";

export function* fetchFriends() {
  try {
    const response = yield axiosApi.get('/users/friends');
    yield put(friendsSuccess(response.data));
  } catch (e) {
    NotificationManager.error(e.response.data.error || e.response.data.global || e.response.data)
    yield put(friendsFailure(e.response.data));
  }
}

export function* postFriends({payload: data}) {
  try {
    const response = yield axiosApi.post('/users/friends', data);
    yield put(postFriendsSuccess());
    yield put(eventsRequest())
    yield put(friendsRequest());
    NotificationManager.success(response.data.message);
    yield put(historyPush('/'));
  } catch (e) {
    NotificationManager.error(e.response.data.error || e.response.data.global || e.response.data)
    yield put(postFriendsFailure(e.response.data));
  }
}

export function* deleteFriend({payload: data}) {
  try {
    const response = yield axiosApi.delete('/users/friends', {data});
    yield put(deleteFriendsSuccess());
    yield put(eventsRequest())
    yield put(friendsRequest());
    NotificationManager.success(response.data.message);
    yield put(historyPush('/'));
  } catch (e) {
    NotificationManager.error(e.response.data.error || e.response.data.global || e.response.data)
    yield put(postFriendsFailure(e.response.data));
  }
}

const friendsSagas = [
  takeEvery(friendsRequest, fetchFriends),
  takeEvery(postFriendsRequest, postFriends),
  takeEvery(deleteFriendsRequest, deleteFriend)
];

export default friendsSagas