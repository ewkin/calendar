import {put, takeEvery} from 'redux-saga/effects';

import {
  clearError, facebookLoginRequest, googleLoginRequest,
  loginFailure, loginRequest,
  loginSuccess, logoutRequest,
  logoutSuccess,
  registerFailure, registerRequest,
  registerSuccess
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {NotificationManager} from "react-notifications";


export function* registerUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users', userData);

    yield put(registerSuccess(response.data));
    yield put(historyPush('/'));
  } catch (error) {
    yield put(registerFailure(error.response.data));
  }
}

export function* loginUser({payload: userData}) {
  try {
    const response = yield axiosApi.post('/users/sessions', userData);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    NotificationManager.success(response.data.message)
  } catch (error) {
    yield put(loginFailure(error.response.data));
  }
}

export function* facebookLogin({payload: data}) {
  try {
    const response = yield axiosApi.post('/users/facebookLogin', data);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    NotificationManager.success('Login successful');
  } catch (e) {
    yield put(loginFailure(e.response.data));
    NotificationManager.error('Login failed');
  }
}

export function* googleLogin({payload: data}) {
  try {
    const body = {
      tokenId: data.tokenId,
      googleId: data.googleId
    };
    const response = yield axiosApi.post('/users/googleLogin', body);
    yield put(loginSuccess(response.data.user));
    yield put(historyPush('/'));
    NotificationManager.success('Login successful');
  } catch (e) {
    yield put(loginFailure(e.response.data));
    NotificationManager.error('Login failed');
  }
}

export function* clearErrors() {
  yield put(clearError());
}

export function* logoutUser() {

  try {

    yield axiosApi.delete('/users/sessions');
    yield put(logoutSuccess());
    yield put(historyPush('/'));
  } catch (e) {
    NotificationManager.error('Could not logout');
  }

}

const userSagas = [
  takeEvery(registerRequest, registerUser),
  takeEvery(loginRequest, loginUser),
  takeEvery(logoutRequest, logoutUser),
  takeEvery(facebookLoginRequest, facebookLogin),
  takeEvery(googleLoginRequest, googleLogin),
  takeEvery(clearError, clearErrors)
];

export default userSagas