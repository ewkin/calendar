import {combineReducers} from "redux";
import createSagaMiddleware from "redux-saga";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import {configureStore} from "@reduxjs/toolkit";
import rootSaga from "./rootSaga";
import axiosApi from "../axiosApi";
import userSlice, {initialState} from "./slices/userSlice";
import eventsSlice from "./slices/eventSlice";
import friendsSlice from "./slices/friendsSlice";


const rootReducer = combineReducers({
  users: userSlice.reducer,
  events: eventsSlice.reducer,
  friends: friendsSlice.reducer,
});


const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: rootReducer,
  middleware: [sagaMiddleware],
  devTools: true,
  preloadedState: loadFromLocalStorage(),
})

sagaMiddleware.run(rootSaga);

store.subscribe(() => {
  saveToLocalStorage({
    users: {
      ...initialState,
      user: store.getState().users.user
    }
  })
});

axiosApi.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = store.getState().users.user.token;
  } catch (e) {
    //no token exists
  }
  return config;
});

axiosApi.interceptors.response.use(res => res, e => {
  if (!e.response) {
    e.response = {data: {global: 'No internet'}}
  }
  throw e
});


export default store;