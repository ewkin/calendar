const {createSlice} = require("@reduxjs/toolkit");

const initialState = {
  eventsLoading: false,
  eventsError: null,
  postEventsLoading: false,
  postEventsError: null,
  deleteEventsLoading: false,
  deleteEventsError: null,
  events: [],
};

const name = 'events'

const eventsSlice = createSlice({
  name,
  initialState,
  reducers: {
    eventsRequest: state => {
      state.eventsLoading = true;
    },
    eventsSuccess: (state, {payload: events}) => {
      state.eventsLoading = false;
      state.events = events;
    },
    eventsFailure: (state, {payload: error}) => {
      state.eventsLoading = false;
      state.eventsError = error;
    },
    postEventsRequest: state => {
      state.postEventsLoading = true;
    },
    postEventsSuccess: (state) => {
      state.postEventsLoading = false;
    },
    postEventsFailure: (state, {payload: error}) => {
      state.postEventsLoading = false;
      state.postEventsError = error;
    },
    deleteEventsRequest: state => {
      state.deleteEventsLoading = true;
    },
    deleteEventsSuccess: (state) => {
      state.deleteEventsLoading = false;
    },
    deleteEventsFailure: (state, {payload: error}) => {
      state.deleteEventsLoading = false;
      state.deleteEventsError = error;
    },
  }
});

export default eventsSlice