const {createSlice} = require("@reduxjs/toolkit");

export const initialState = {
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null,
  user: null,
};

const name = 'users'

const userSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerRequest: state => {
      state.registerLoading = true;
    },
    registerSuccess: (state, {payload: user}) => {
      state.registerLoading = false;
      state.user = user;
    },
    registerFailure: (state, {payload: error}) => {
      state.registerLoading = false;
      state.registerError = error;
    },
    loginRequest: state => {
      state.loginLoading = true;
    },
    loginSuccess: (state, {payload: user}) => {
      state.loginLoading = false;
      state.user = user;
    },
    logoutRequest: () => {
    },
    facebookLoginRequest: (state) => {
      state.loginLoading = true;
    },
    googleLoginRequest: (state) => {
      state.loginLoading = true;
    },

    loginFailure: (state, {payload: error}) => {
      state.loginLoading = false;
      state.loginError = error;
    },
    logoutSuccess: state => {
      state.user = null;
    },
    clearErrorRequest: () => {
    },
    clearError: state => {
      state.registerError = null;
      state.loginError = null
    }
  }
});

export default userSlice