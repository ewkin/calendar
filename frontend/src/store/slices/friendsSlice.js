const {createSlice} = require("@reduxjs/toolkit");

const initialState = {
  friendsLoading: false,
  friendsError: null,
  postFriendsLoading: false,
  postFriendsError: null,
  deleteFriendsLoading: false,
  deleteFriendsError: null,
  friends: [],
};

const name = 'friends'

const friendsSlice = createSlice({
  name,
  initialState,
  reducers: {
    friendsRequest: state => {
      state.friendsLoading = true;
    },
    friendsSuccess: (state, {payload: friends}) => {
      state.friendsLoading = false;
      state.friends = friends;
    },
    friendsFailure: (state, {payload: error}) => {
      state.friendsLoading = false;
      state.friendsError = error;
    },
    postFriendsRequest: state => {
      state.postFriendsLoading = true;
    },
    postFriendsSuccess: (state) => {
      state.postFriendsLoading = false;
    },
    postFriendsFailure: (state, {payload: error}) => {
      state.postfriendsLoading = false;
      state.postfriendsError = error;
    },
    deleteFriendsRequest: state => {
      state.deleteFriendsLoading = true;
    },
    deleteFriendsSuccess: (state) => {
      state.deleteFriendsLoading = false;
    },
    deleteFriendsFailure: (state, {payload: error}) => {
      state.deletefriendsLoading = false;
      state.deletefriendsError = error;
    },
  }
});

export default friendsSlice