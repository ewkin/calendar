import friendsSlice from "../slices/friendsSlice";


export const {
  friendsRequest,
  friendsSuccess,
  friendsFailure,
  postFriendsFailure,
  postFriendsSuccess,
  postFriendsRequest,
  deleteFriendsRequest,
  deleteFriendsSuccess,
  deleteFriendsFailure


} = friendsSlice.actions;
