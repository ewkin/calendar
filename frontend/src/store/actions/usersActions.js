import userSlice from "../slices/userSlice";


export const {
  registerRequest,
  registerSuccess,
  registerFailure,
  loginRequest,
  loginSuccess,
  loginFailure,
  clearError,
  logoutSuccess,
  logoutRequest,
  clearErrorRequest,
  googleLoginRequest,
  facebookLoginRequest

} = userSlice.actions;
