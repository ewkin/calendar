import eventsSlice from "../slices/eventSlice";


export const {
  eventsRequest,
  eventsSuccess,
  eventsFailure,
  postEventsFailure,
  postEventsSuccess,
  postEventsRequest,
  deleteEventsFailure,
  deleteEventsSuccess,
  deleteEventsRequest


} = eventsSlice.actions;
