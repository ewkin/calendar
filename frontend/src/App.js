import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Calendar from "./containers/Calendar/Calendar";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/UI/Layout/Layout";
import {useSelector} from "react-redux";
import NewEvent from "./containers/NewEvent/NewEvent";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props}/> : <Redirect to={redirectTo}/>;
};

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Layout>
      <Switch>
        <ProtectedRoute
          isAllowed={user}
          redirectTo='/login'
          exact
          path="/"
          component={Calendar}
        />
        <Route path="/register" component={Register}/>
        <Route path="/new-event" component={NewEvent}/>
        <Route path="/login" component={Login}/>
        <Route render={() => <h1>Not found</h1>}/>
      </Switch>
    </Layout>
  );
};

export default App;