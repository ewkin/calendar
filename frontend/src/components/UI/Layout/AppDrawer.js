import React from 'react';
import {Drawer, makeStyles, Toolbar} from "@material-ui/core";


const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
}));


const AppDrawer = ({children}) => {
  const classes = useStyles();


  return (
    <Drawer
      className={classes.drawer}
      classes={{paper: classes.drawerPaper}}
      variant={"permanent"}
      open>
      <Toolbar/>
      {children}
    </Drawer>
  );
};

export default AppDrawer;