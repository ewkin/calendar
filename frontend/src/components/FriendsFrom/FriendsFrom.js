import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../UI/Form/FormElement";
import {Button, makeStyles} from "@material-ui/core";
import {postFriendsRequest} from "../../store/actions/friendsActions";
import {useDispatch} from "react-redux";

const useStyles = makeStyles(theme => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function getModalStyle() {
  const top = 50;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}


const FriendsFrom = ({setOpen}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [modalStyle] = React.useState(getModalStyle);

  const [state, setState] = useState({
    email: ''
  });

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(postFriendsRequest({...state}));
    setOpen(false);
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };


  return (
    <div style={modalStyle} className={classes.paper}>
      <form onSubmit={submitFormHandler} noValidate>
        <Grid container direction="column" spacing={2}>
          <FormElement
            required
            type="email"
            label="Email"
            name="email"
            value={state.email}
            onChange={inputChangeHandler}
          />
          <Grid item xs>
            <Button type="submit" color="primary" variant="contained">
              Send friend request
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default FriendsFrom;