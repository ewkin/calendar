import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const EventForm = ({onSubmit, error, loading}) => {
  const [state, setState] = useState({
    name: '',
    date: '',
    duration: '',
  });

  const submitFormHandler = e => {
    e.preventDefault();
    onSubmit({...state});
  };

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => ({
      ...prevState, [name]: value
    }));
  };


  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }


  return (
    <form onSubmit={submitFormHandler} noValidate>
      <Grid container direction="column" spacing={2}>
        <FormElement
          required
          label="Name"
          name="name"
          value={state.name}
          onChange={inputChangeHandler}
          error={getFieldError('name')}
        />
        <FormElement
          required
          type="datetime-local"
          name="date"
          value={state.date}
          onChange={inputChangeHandler}
          error={getFieldError('date')}

        />
        <FormElement
          required
          type="number"
          label="Duration"
          name="duration"
          value={state.duration}
          onChange={inputChangeHandler}
          error={getFieldError('duration')}
        />

        <Grid item xs>
          <ButtonWithProgress type="submit" color="primary" variant="contained" loading={loading} disabled={loading}>
            Create
          </ButtonWithProgress>
        </Grid>
      </Grid>
    </form>
  );
};

export default EventForm;